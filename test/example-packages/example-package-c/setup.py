import setuptools

setuptools.setup(
    name="pip-compile-cross-platform-example-package-c",
    version="0.0.3",
    author="Mitchell Hentges",
    author_email="mitch9654@gmail.com",
    description="Example dummy package for pip-compile-cross-platform testing",
    url="https://gitlab.com/mitchhentges/pip-compile-cross-platform",
)
