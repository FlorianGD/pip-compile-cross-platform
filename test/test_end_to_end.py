import os
from pathlib import Path
from tempfile import TemporaryDirectory
from textwrap import dedent
from typing import List, Optional
from unittest.mock import patch

import pytest
import tomli
import tomli_w
from packaging.specifiers import SpecifierSet
from pip_requirements_parser import RequirementsFile

from pip_compile_cross_platform import FRONTMATTER, create_poetry_project, main


def _run_pip_compile_cross_platform(
    work_dir: Path,
    input_files: Optional[List[Path]] = None,
    output_file: Optional[Path] = None,
    return_requirements: bool = True,
):
    create_poetry_project_orig = create_poetry_project

    def create_poetry_project_test_pypi(work_dir: Path, min_python_version: str):
        create_poetry_project_orig(work_dir, min_python_version)

        with open(work_dir / "pyproject.toml", "rb") as pyproject_file:
            pyproject = tomli.load(pyproject_file)

        pyproject["tool"]["poetry"]["source"] = [
            {
                "name": "pypi-test",
                "url": "https://test.pypi.org/simple/",
            }
        ]

        with open(work_dir / "pyproject.toml", "wb") as pyproject_file:
            tomli_w.dump(pyproject, pyproject_file)

    with patch(
        "pip_compile_cross_platform.create_poetry_project",
        new=create_poetry_project_test_pypi,
    ):
        orig_cwd = Path.cwd()
        os.chdir(work_dir)
        main(input_files or [], output_file, "3.6")
        os.chdir(orig_cwd)

    if return_requirements:
        return RequirementsFile.from_file(
            str(work_dir / "requirements.txt"), include_nested=True
        ).requirements


def test_minimal():
    with TemporaryDirectory() as work_dir_str:
        work_dir = Path(work_dir_str)

        with open(work_dir / "requirements.in", "w") as requirements_in_file:
            requirements_in_file.write(
                "pip-compile-cross-platform-example-package-a==0.0.3"
            )

        requirements = _run_pip_compile_cross_platform(work_dir)
        assert len(requirements) == 1
        assert (
            requirements[0].req.name == "pip-compile-cross-platform-example-package-a"
        )
        assert requirements[0].req.specifier == SpecifierSet("==0.0.3")

        with open(work_dir / "requirements.txt", "r") as requirements_txt_file:
            contents = requirements_txt_file.read()
            frontmatter = FRONTMATTER.format(
                command="pip-compile-cross-platform --min-python-version 3.6"
            ).splitlines()
            assert contents.splitlines()[: len(frontmatter)] == frontmatter


def test_transitive_requirement():
    with TemporaryDirectory() as work_dir_str:
        work_dir = Path(work_dir_str)

        with open(work_dir / "requirements.in", "w") as requirements_in_file:
            requirements_in_file.write(
                "pip-compile-cross-platform-example-package-b==0.0.3"
            )

        requirements = _run_pip_compile_cross_platform(work_dir)
        assert len(requirements) == 2
        assert (
            requirements[0].req.name == "pip-compile-cross-platform-example-package-a"
        )
        assert requirements[0].req.specifier == SpecifierSet("==0.0.3")
        assert (
            requirements[1].req.name == "pip-compile-cross-platform-example-package-b"
        )
        assert requirements[1].req.specifier == SpecifierSet("==0.0.3")


def test_setup_py():
    with TemporaryDirectory() as work_dir_str:
        work_dir = Path(work_dir_str)

        with open(work_dir / "setup.py", "w") as setup_py_file:
            setup_py_file.write(
                dedent(
                    """\
            import setuptools

            setuptools.setup(
                name="example",
                version="0.0.1",
                install_requires=["pip-compile-cross-platform-example-package-a==0.0.3"],
            )
            """
                )
            )

        requirements = _run_pip_compile_cross_platform(work_dir)
        assert len(requirements) == 1
        assert (
            requirements[0].req.name == "pip-compile-cross-platform-example-package-a"
        )
        assert requirements[0].req.specifier == SpecifierSet("==0.0.3")


def test_choose_default_requirements_in_file():
    with TemporaryDirectory() as work_dir_str:
        work_dir = Path(work_dir_str)

        with open(work_dir / "requirements.in", "w") as requirements_in_file:
            requirements_in_file.write(
                "pip-compile-cross-platform-example-package-a==0.0.3"
            )

        with open(work_dir / "requirements.test.in", "w") as requirements_in_file:
            requirements_in_file.write(
                "pip-compile-cross-platform-example-package-c==0.0.3"
            )

        requirements = _run_pip_compile_cross_platform(work_dir)
        assert len(requirements) == 1
        assert (
            requirements[0].req.name == "pip-compile-cross-platform-example-package-a"
        )
        assert requirements[0].req.specifier == SpecifierSet("==0.0.3")


def test_multiple_input_files():
    with TemporaryDirectory() as work_dir_str:
        work_dir = Path(work_dir_str)

        with open(work_dir / "requirements.dev.in", "w") as requirements_in_file:
            requirements_in_file.write(
                "pip-compile-cross-platform-example-package-a==0.0.3"
            )

        with open(work_dir / "requirements.test.in", "w") as requirements_in_file:
            requirements_in_file.write(
                "pip-compile-cross-platform-example-package-c==0.0.3"
            )

        requirements = _run_pip_compile_cross_platform(
            work_dir,
            [work_dir / "requirements.dev.in", work_dir / "requirements.test.in"],
        )
        assert len(requirements) == 2
        assert (
            requirements[0].req.name == "pip-compile-cross-platform-example-package-a"
        )
        assert requirements[0].req.specifier == SpecifierSet("==0.0.3")
        assert (
            requirements[1].req.name == "pip-compile-cross-platform-example-package-c"
        )
        assert requirements[1].req.specifier == SpecifierSet("==0.0.3")


def test_output_name_detection():
    with TemporaryDirectory() as work_dir_str:
        work_dir = Path(work_dir_str)

        with open(work_dir / "requirements.test.in", "w") as requirements_in_file:
            requirements_in_file.write(
                "pip-compile-cross-platform-example-package-a==0.0.3"
            )

        _run_pip_compile_cross_platform(
            work_dir, [work_dir / "requirements.test.in"], return_requirements=False
        )
        assert (work_dir / "requirements.test.txt").exists()


def test_explicit_output_name():
    with TemporaryDirectory() as work_dir_str:
        work_dir = Path(work_dir_str)

        with open(work_dir / "requirements.test.in", "w") as requirements_in_file:
            requirements_in_file.write(
                "pip-compile-cross-platform-example-package-a==0.0.3"
            )

        _run_pip_compile_cross_platform(
            work_dir,
            [work_dir / "requirements.test.in"],
            work_dir / "pip.lockfile",
            return_requirements=False,
        )
        requirements = RequirementsFile.from_file(
            str(work_dir / "pip.lockfile"), include_nested=True
        ).requirements
        assert len(requirements) == 1
        assert (
            requirements[0].req.name == "pip-compile-cross-platform-example-package-a"
        )
        assert requirements[0].req.specifier == SpecifierSet("==0.0.3")


def test_maintain_pinned_version():
    with TemporaryDirectory() as work_dir_str:
        work_dir = Path(work_dir_str)

        with open(work_dir / "requirements.in", "w") as requirements_in_file:
            requirements_in_file.write(
                "pip-compile-cross-platform-example-package-a==0.0.2"
            )

        _run_pip_compile_cross_platform(work_dir, return_requirements=False)

        with open(work_dir / "requirements.in", "w") as requirements_in_file:
            requirements_in_file.write(
                "pip-compile-cross-platform-example-package-a>=0.0.2"
            )

        requirements = _run_pip_compile_cross_platform(work_dir)
        assert len(requirements) == 1
        assert (
            requirements[0].req.name == "pip-compile-cross-platform-example-package-a"
        )
        assert requirements[0].req.specifier == SpecifierSet("==0.0.2")

        # Re-generating lockfile from scratch should choose the newer version
        (work_dir / "requirements.txt").unlink()
        requirements = _run_pip_compile_cross_platform(work_dir)
        assert len(requirements) == 1
        assert (
            requirements[0].req.name == "pip-compile-cross-platform-example-package-a"
        )
        assert requirements[0].req.specifier == SpecifierSet("==0.0.3")


def test_detect_conflicts(capsys):
    with TemporaryDirectory() as work_dir_str:
        work_dir = Path(work_dir_str)

        with open(work_dir / "requirements.in", "w") as requirements_in_file:
            requirements_in_file.write(
                dedent(
                    """\
                pip-compile-cross-platform-example-package-a==0.0.3
                pip-compile-cross-platform-example-package-b==0.0.2
            """
                )
            )

        with pytest.raises(Exception):
            _run_pip_compile_cross_platform(work_dir)

        text_output = capsys.readouterr()
        assert "SolveFailure" in text_output.err
